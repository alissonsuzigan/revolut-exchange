module.exports = {
  extends: ['airbnb', 'airbnb/hooks', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  parserOptions: {
    project: './tsconfig.json'
  },
  env: {
    browser: true,
    jest: true
  },
  settings: {
    'import/resolver': {
      node: { extensions: ['.js', '.jsx', '.ts', '.tsx'] }
    }
  },
  rules: {
    'max-len': [2, {'code': 120, 'ignoreUrls': true}],
    'react/jsx-filename-extension': [2, { extensions: ['.ts', '.tsx'] }],
    'react/jsx-one-expression-per-line': [0],
    '@typescript-eslint/indent': [2, 2],
    'object-curly-newline': [2, { 'consistent': true }]
  },
  "overrides": [{
    "files": ["*.test.ts","*.test.tsx"],
    "rules": {
      "@typescript-eslint/no-non-null-assertion": "off",
    }
  }]
}