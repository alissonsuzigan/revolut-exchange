import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          margin: 0,
          backgroundColor: '#EEE',
        },
      },
    },
  },
});

export default theme;
