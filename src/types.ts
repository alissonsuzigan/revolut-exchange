export interface Pocket {
  symbol: string;
  label: string;
  value: number;
}

export interface Wallet {
  activeKey: string;
  pockets: Record<string, Pocket>;
}

export interface ExchangeData {
  symbol: string;
  value: number;
}

export interface Exchange {
  source: ExchangeData;
  target: ExchangeData;
  rates: Record<string, number>;
}

export interface ExchangeResponse {
  base: string;
  date: string;
  rates: Record<string, number>;
}
