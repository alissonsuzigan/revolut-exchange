/* istanbul ignore file */
import React from 'react';
import { History } from 'history';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// Components
import Header from '../components/header';
import PocketDetail from '../components/pocket-detail';
import PocketList from '../components/pocket-list';

const useStyles = makeStyles({
  container: {
    height: '100vh',
  },

  wrapper: {
    display: 'grid',
    gridTemplateRows: 'auto 3fr 2fr',
    width: '100%',
    maxWidth: '640px',
    height: '100%',
    maxHeight: '840px',
    overflow: 'hidden',
    boxShadow: '0 4px 10px rgba(0, 0, 0, 0.5)',
  },
});

const Pockets = ({ history }: {history: History}): JSX.Element => {
  const classes = useStyles();
  const gotoRoute = (path: string): void => history.push(path);

  return (
    <Grid container className={classes.container} justify="center" alignItems="center">
      <div className={classes.wrapper}>
        <Header />
        <PocketDetail gotoRoute={gotoRoute} />
        <PocketList />
      </div>
    </Grid>
  );
};

export default Pockets;
