import React from 'react';
import { renderHook, act, HookResult } from '@testing-library/react-hooks';
import { ExchangeProvider, useExchange, UseExchange } from './exchange';

// Helper method to get useExchange content
const renderUseExchangeHook = (): HookResult<UseExchange> => {
  const wrapper = ({ children }: { children?: React.ReactNode }): JSX.Element => (
    <ExchangeProvider>{children}</ExchangeProvider>
  );
  const { result } = renderHook((): UseExchange => useExchange(), { wrapper });
  return result;
};

describe('useExchange()', (): void => {
  it('returns exchange data structure', (): void => {
    const result = renderUseExchangeHook();
    const { exchange } = result.current;
    // Source structure
    expect(exchange).toHaveProperty('source');
    expect(exchange.source).toEqual({ symbol: 'GBP', value: 0 });
    // Target structure
    expect(exchange).toHaveProperty('target');
    expect(exchange.target).toEqual({ symbol: 'USD', value: 0 });
    // Rates
    expect(exchange).toHaveProperty('rates');
  });

  it('changes source data', (): void => {
    const result = renderUseExchangeHook();
    const { exchange, setSource } = result.current;
    expect(exchange.source).toEqual({ symbol: 'GBP', value: 0 });
    act(() => {
      setSource('EUR', 100);
    });
    expect(result.current.exchange.source).toEqual({ symbol: 'EUR', value: 100 });
  });

  it('changes target data', (): void => {
    const result = renderUseExchangeHook();
    const { exchange, setTarget } = result.current;

    expect(exchange.target).toEqual({ symbol: 'USD', value: 0 });
    act(() => {
      setTarget('GBP', 90);
    });
    expect(result.current.exchange.target).toEqual({ symbol: 'GBP', value: 90 });
  });

  it('changes source and target values', (): void => {
    const result = renderUseExchangeHook();
    const { exchange, setValues } = result.current;

    expect(exchange.source).toEqual({ symbol: 'GBP', value: 0 });
    expect(exchange.target).toEqual({ symbol: 'USD', value: 0 });
    act(() => {
      setValues(10, 12.5);
    });
    expect(result.current.exchange.source).toEqual({ symbol: 'GBP', value: 10 });
    expect(result.current.exchange.target).toEqual({ symbol: 'USD', value: 12.5 });
  });

  it('fetch rates from API', async (): Promise<void> => {
    const result = renderUseExchangeHook();
    const { exchange, fetchRates } = result.current;

    expect(exchange.rates).toEqual({});
    await act(async () => {
      fetchRates('USD', ['GBP', 'EUR']);
    });
    // expect(result.current.exchange.rates).toEqual({});
  });
});
