import React from 'react';
import { Wallet, Pocket, ExchangeData } from '../types';
import { walletMock } from '../utils/mocks';
/**
 * Defines Pocket initial data and context
 */
type PocketContextType = [
  Wallet,
  React.Dispatch<React.SetStateAction<Wallet>>
];

const INITIAL_DATA: Wallet = walletMock;
const PocketContext = React.createContext<PocketContextType>([INITIAL_DATA, (): void => {}]);

/**
 * Defines Pocket context provider
 */
interface PocketProviderProps {
  children: React.ReactNode;
  data?: Wallet;
}

export const PocketProvider = ({ children, data = INITIAL_DATA }: PocketProviderProps): JSX.Element => {
  const initialState = data;
  const [pocket, setPocket] = React.useState(initialState);
  const contextValue = React.useMemo((): PocketContextType => ([pocket, setPocket]), [pocket]);

  return (
    <PocketContext.Provider value={contextValue}>
      {children}
    </PocketContext.Provider>
  );
};

/**
 * Defines Pocket custom hook
 */
export interface UsePocket {
  wallet: Wallet;
  getActivePocket: () => Pocket;
  getPocketBySymbol: (symbol: string) => Pocket;
  setActivePocket: (symbol: string) => void;
  setPocketExchange: (source: ExchangeData, target: ExchangeData) => void;
  getPrevNextSymbol: (symbol: string) => { prev: string; next: string };
}

export const usePocket = (): UsePocket => {
  const [wallet, setWallet] = React.useContext(PocketContext);

  const getActivePocket = (): Pocket => {
    const { activeKey, pockets } = wallet;
    return pockets[activeKey];
  };

  const getPocketBySymbol = (symbol: string): Pocket => {
    const { pockets } = wallet;
    return pockets[symbol];
  };

  const setActivePocket = (symbol: string): void => {
    setWallet((prevWallet) => ({ ...prevWallet, activeKey: symbol }));
  };

  const setPocketExchange = (source: ExchangeData, target: ExchangeData): void => {
    const pocketSource = { ...wallet.pockets[source.symbol] };
    const pocketTarget = { ...wallet.pockets[target.symbol] };
    pocketSource.value -= source.value;
    pocketTarget.value += target.value;

    setWallet((prevWallet) => ({
      ...prevWallet,
      pockets: {
        ...prevWallet.pockets,
        [pocketSource.symbol]: pocketSource,
        [pocketTarget.symbol]: pocketTarget,
      },
    }));
  };

  const getPrevNextSymbol = (symbol: string): { prev: string; next: string } => {
    const { pockets } = wallet;
    const pocketKeys = Object.keys(pockets);
    const idx = pocketKeys.indexOf(symbol);
    const prevIdx = idx > 0 ? idx - 1 : pocketKeys.length - 1;
    const nextIdx = idx < pocketKeys.length - 1 ? idx + 1 : 0;
    return {
      prev: pocketKeys[prevIdx],
      next: pocketKeys[nextIdx],
    };
  };

  return { wallet, getActivePocket, getPocketBySymbol, setActivePocket, getPrevNextSymbol, setPocketExchange };
};
