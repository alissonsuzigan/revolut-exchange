import React from 'react';
import { renderHook, HookResult, act } from '@testing-library/react-hooks';
import { PocketProvider, usePocket, UsePocket } from './pocket';
import { walletMock } from '../utils/mocks';

// Helper method to get usePocket content
const renderUsePocketHook = (): HookResult<UsePocket> => {
  const wrapper = ({ children }: { children?: React.ReactNode }): JSX.Element => (
    <PocketProvider data={walletMock}>{children}</PocketProvider>
  );
  const { result } = renderHook((): UsePocket => usePocket(), { wrapper });
  return result;
};

describe('usePocket()', (): void => {
  const result = renderUsePocketHook();
  const { wallet, getActivePocket, getPocketBySymbol, getPrevNextSymbol, setActivePocket } = result.current;

  it('returns wallet with active key: GBP', (): void => {
    expect(wallet.activeKey).toBe('GBP');
  });

  it('returns wallet with pockets symbols: USD, EUR, GBP', (): void => {
    expect(wallet.pockets).toHaveProperty('USD');
    expect(wallet.pockets).toHaveProperty('EUR');
    expect(wallet.pockets).toHaveProperty('GBP');
  });

  it('returns wallet with EUR pocket properties', (): void => {
    expect(wallet.pockets.EUR).toHaveProperty('symbol');
    expect(wallet.pockets.EUR).toHaveProperty('label');
    expect(wallet.pockets.EUR).toHaveProperty('value');
  });

  it('gets the active pocket data', (): void => {
    const pocket = getActivePocket();
    expect(pocket.label).toBe('British Pound Sterling');
    expect(pocket.symbol).toBe('GBP');
    expect(pocket.value).toBe(800);
  });

  it('gets pocket data by USD symbol', (): void => {
    const pocket = getPocketBySymbol('USD');
    expect(pocket.label).toBe('United States Dollar');
    expect(pocket.symbol).toBe('USD');
    expect(pocket.value).toBe(1500);
  });

  it('gets prev and next symbols', (): void => {
    const { prev, next } = getPrevNextSymbol('GBP');
    expect(prev).toBe('USD');
    expect(next).toBe('EUR');
  });

  it('sets the active pocket data', (): void => {
    expect(wallet.activeKey).toBe('GBP');
    act(() => setActivePocket('EUR'));
    expect(result.current.wallet.activeKey).toBe('EUR');
  });
});
