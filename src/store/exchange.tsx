import React from 'react';
import { getExchangeRates } from '../utils/currency';
import { Exchange } from '../types';

/**
 * Defines Exchange initial data and context
 */
type ExchangeContextType = [
  Exchange,
  React.Dispatch<React.SetStateAction<Exchange>>
];

const INITIAL_DATA: Exchange = {
  source: {
    symbol: 'GBP',
    value: 0,
  },
  target: {
    symbol: 'USD',
    value: 0,
  },
  rates: {},
};
const ExchangeContext = React.createContext<ExchangeContextType>([INITIAL_DATA, (): void => {}]);

/**
 * Defines Exchange context provider
 */
interface ExchangeProviderProps {
  children: React.ReactNode;
  data?: Exchange;
}

export const ExchangeProvider = ({ children, data = INITIAL_DATA }: ExchangeProviderProps): JSX.Element => {
  const initialState = data;
  const [exchange, setExchange] = React.useState(initialState);
  const contextValue = React.useMemo((): ExchangeContextType => ([exchange, setExchange]), [exchange]);

  return (
    <ExchangeContext.Provider value={contextValue}>
      {children}
    </ExchangeContext.Provider>
  );
};

/**
 * Defines Exchange custom hook
 */
export interface UseExchange {
  exchange: Exchange;
  setSource: (symbol: string, value: number) => void;
  setTarget: (symbol: string, value: number) => void;
  setValues: (source: number, target: number) => void;
  fetchRates: (base: string, symbols: string[]) => Promise<void>;
}

export const useExchange = (): UseExchange => {
  const [exchange, setExchange] = React.useContext(ExchangeContext);

  const setSource = (symbol: string, value: number): void => (
    setExchange((state) => ({ ...state, source: { symbol, value } }))
  );

  const setTarget = (symbol: string, value: number): void => (
    setExchange((state) => ({ ...state, target: { symbol, value } }))
  );

  const setValues = (sourceValue: number, targetValue: number): void => setExchange((state) => {
    const newExchange = {
      ...state,
      source: {
        symbol: state.source.symbol,
        value: sourceValue,
      },
      target: {
        symbol: state.target.symbol,
        value: targetValue,
      },
    };
    return newExchange;
  });

  const fetchRates = async (base: string, symbols: string[]): Promise<void> => {
    const filteredSymbos = symbols.filter(symbol => symbol !== base);
    const data = await getExchangeRates(base, filteredSymbos);
    data.rates[base] = 1;
    setExchange((state) => ({ ...state, rates: data.rates }));
  };

  return { exchange, setSource, setTarget, setValues, fetchRates };
};
