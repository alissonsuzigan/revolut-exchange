import { format, getExchangeRates, sanitizeCurrency } from '../currency';

describe('Currency methods', () => {
  describe('format()', () => {
    it('returns formatted EUR currency', () => {
      const value = format('EUR', 2500);
      expect(value).toBe('€2,500.00');
    });

    it('returns formatted USD currency', () => {
      const value = format('USD', 1500.7);
      expect(value).toBe('$1,500.70');
    });

    it('returns formatted GBP currency', () => {
      const value = format('GBP', 800.55);
      expect(value).toBe('£800.55');
    });

    it('returns formatted BRL currency', () => {
      const value = format('BRL', 2800);
      expect(value).toBe('R$2,800.00');
    });
  });


  describe('sanitizeCurrency()', () => {
    it('returns value with zero before dot', () => {
      const value = sanitizeCurrency('.80');
      expect(value).toBe('0.80');
    });

    it('returns value with 2 decimals', () => {
      const value = sanitizeCurrency('10.999999');
      expect(value).toBe('10.99');
    });

    it('returns value removing the second dot and everything else', () => {
      const value = sanitizeCurrency('255.8.366');
      expect(value).toBe('255.8');
    });

    it('returns value with no special chars, except dot', () => {
      const value = sanitizeCurrency('!1@2#3$4%5^6&7*8.(9)0_-+={}[]<,>?/|~`');
      expect(value).toBe('12345678.90');
    });
  });


  describe('getExchangeRates()', () => {
    it('fetches API and return data', async () => {
      const value = await getExchangeRates('EUR', ['USD', 'GBP']);
      expect(value).toHaveProperty('base');
      expect(value).toHaveProperty('date');
      expect(value).toHaveProperty('rates');
      expect(value.rates).toHaveProperty('GBP');
      expect(value.rates).toHaveProperty('USD');
    });

    it('fetches API and return error', async () => {
      const value = await getExchangeRates('XXX', ['123', '123']);
      expect(value).toEqual({ error: 'Base \'XXX\' is not supported.' });
    });
  });
});
