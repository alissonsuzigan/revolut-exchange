import { ExchangeResponse } from '../types';

/**
 * Formats value by currency
 */
export const format = (currency: string, value: number): string => (
  Intl.NumberFormat('en-US', { style: 'currency', currency }).format(value)
);

/**
 * Sanitize value using currency format
 */
export const sanitizeCurrency = (value: string): string => (
  value
    .replace(/^\./, '0.') // if starts with '.' replace to '0.'
    .replace(/[^\d.]+/g, '') // remove everething except digits and dots
    .replace(/^(\d+\.?\d{0,2})(.*)/g, '$1') // remove everething after first dot and to digits
);

/**
 * Get exchange rates from API
 */
export const getExchangeRates = async (base: string, symbols: string[]): Promise<ExchangeResponse> => {
  const api = 'https://api.exchangeratesapi.io/latest';
  const params = `?base=${base}&symbols=${symbols.join(',')}`;

  try {
    const response = await fetch(`${api}/${params}`);
    const result = await response.json();
    return result;
  } catch (response) {
    throw Error('response.statusText');
  }
};
