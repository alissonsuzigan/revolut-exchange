export const walletMock = {
  activeKey: 'GBP',
  pockets: {
    GBP: {
      symbol: 'GBP',
      label: 'British Pound Sterling',
      value: 800,
    },
    EUR: {
      symbol: 'EUR',
      label: 'Euro',
      value: 1000,
    },
    USD: {
      symbol: 'USD',
      label: 'United States Dollar',
      value: 1500,
    },
  },
};

export const exchangeMock = {
  source: {
    symbol: 'GBP',
    value: 0,
  },
  target: {
    symbol: 'USD',
    value: 0,
  },
  rates: {
    EUR: 1.1290122272,
    USD: 1.2293814142,
    GBP: 1.0,
  },
};
