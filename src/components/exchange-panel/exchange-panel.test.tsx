import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ExchangePanel from './exchange-panel';
import { walletMock } from '../../utils/mocks';

const updateValue = jest.fn();

describe('<ExchangePanel />', (): void => {
  it('renders enabled compoennt and calls updateValue() when input is changed', (): void => {
    const { container } = render(
      <ExchangePanel data={walletMock.pockets.EUR} inputValue={100} updateValue={updateValue} />,
    );
    const labels = container.querySelectorAll('label');
    const input = container.querySelector('input');
    expect(labels[0].textContent).toBe('EUR');
    expect(labels[1].textContent).toBe('You have €1,000.00');
    expect(container.querySelector('input')!.value).toBe('100');
    expect(container.querySelector('span')!.textContent).toBe('');

    fireEvent.change(input!, { target: { value: 120 } });
    expect(updateValue).toHaveBeenCalledTimes(1);
    expect(updateValue).toHaveBeenCalledWith('EUR', 120);
  });

  it('renders disabled compoennt and calls updateValue() when input is changed', (): void => {
    const { container } = render(
      <ExchangePanel
        data={walletMock.pockets.USD}
        exchangeRate="$1.00 = €0.92"
        updateValue={updateValue}
        inputValue={200}
        disabled
      />,
    );
    const labels = container.querySelectorAll('label');
    const input = container.querySelector('input');
    expect(labels[0].textContent).toBe('USD');
    expect(labels[1].textContent).toBe('You have $1,500.00');
    expect(container.querySelector('input')!.value).toBe('200');
    expect(container.querySelector('span')!.textContent).toBe('$1.00 = €0.92');

    fireEvent.change(input!, { target: { value: 120 } });
    expect(updateValue).toHaveBeenCalledTimes(1);
    expect(updateValue).toHaveBeenCalledWith('EUR', 120);
  });
});
