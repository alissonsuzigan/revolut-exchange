import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { format, sanitizeCurrency } from '../../utils/currency';
import { Pocket } from '../../types';

const useStyles = makeStyles({
  panel: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  label: {
    minWidth: 160,
  },
  symbol: {
    fontSize: '2rem',
    display: 'block',
  },
  amount: {
    fontSize: '1rem',
    display: 'block',
  },
  value: {
    textAlign: 'right',
  },
  input: {
    appearance: 'none',
    background: 'transparent',
    color: 'inherit',
    fontSize: '2rem',
    width: 160,
    height: 45,
    textAlign: 'right',
    border: 'none',
    outline: 'none',
    '&::-webkit-inner-spin-button, &::-webkit-outer-spin-button': {
      appearance: 'none',
      margin: 0,
    },
  },
  inputInfo: {
    display: 'block',
  },
});

interface Props {
  data: Pocket;
  inputValue: number;
  disabled?: boolean;
  exchangeRate?: string;
  updateValue: (symbol: string, value: number) => void;
}

function ExchangePanel({ data, disabled, exchangeRate, inputValue, updateValue }: Props): JSX.Element {
  const classes = useStyles();
  const [input, setInput] = useState('');
  const inputRef = useRef<HTMLInputElement>(null);

  const onChange = (event: React.FormEvent<HTMLInputElement>): void => {
    if (disabled) return;
    const sanitizedValue = sanitizeCurrency(event.currentTarget.value);
    const value = (Number(sanitizedValue) < data.value) ? sanitizedValue : data.value.toString();
    setInput(value);
    updateValue(data.symbol, Number(value));
  };

  useEffect(() => {
    const value = (inputValue < data.value) ? inputValue : data.value;
    const valueStr = value ? value.toString() : '';
    setInput(valueStr);
    // eslint-disable-next-line
  }, [inputValue]);

  useEffect(() => {
    if (!disabled && inputRef && inputRef.current) {
      inputRef.current.focus();
    }
    // eslint-disable-next-line
  }, []);

  const getInputValue = (): string | number => {
    if (disabled) return inputValue || '';
    return input;
  };

  return (
    <div className={classes.panel}>
      <div className={classes.label}>
        <label className={classes.symbol} htmlFor="source-input">
          {data.symbol}
        </label>
        <label className={classes.amount} htmlFor="source-input">
          You have {format(data.symbol, data.value)}
        </label>
      </div>
      <div className={classes.value}>
        <input
          id="source-input"
          type="tel"
          ref={inputRef}
          className={classes.input}
          disabled={disabled}
          onChange={onChange}
          value={getInputValue()}
        />
        <span className={classes.inputInfo}>{exchangeRate}</span>
      </div>
    </div>
  );
}

export default ExchangePanel;
