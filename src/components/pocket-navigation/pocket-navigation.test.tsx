import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import PocketNavigation from './pocket-navigation';

describe('<PocketNavigation />', (): void => {
  const onNavigate = jest.fn();
  const prev = 'USD';
  const next = 'EUR';

  it('renders properly', (): void => {
    const { container } = render(<PocketNavigation prev={prev} next={next} onNavigate={onNavigate} />);
    const buttons = container.querySelectorAll('button');

    expect(buttons[0].textContent).toBe(prev);
    expect(buttons[1].textContent).toBe(next);
  });

  it('calls `onNavigate()` when it was clicked', (): void => {
    const { container } = render(<PocketNavigation prev={prev} next={next} onNavigate={onNavigate} />);
    const buttons = container.querySelectorAll('button');

    fireEvent.click(buttons[0]);
    expect(onNavigate).toHaveBeenCalledTimes(1);
    expect(onNavigate).toHaveBeenCalledWith(prev);

    fireEvent.click(buttons[1]);
    expect(onNavigate).toHaveBeenCalledTimes(2);
    expect(onNavigate).toHaveBeenCalledWith(next);
  });
});
