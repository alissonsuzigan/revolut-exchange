import React from 'react';
import { makeStyles } from '@material-ui/core';
import { NavigateNext as NextIcon, NavigateBefore as PrevIcon } from '@material-ui/icons';
import { NavButton } from '../custom-buttons';

const useStyles = makeStyles({
  navigation: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: 80,
  },
});

interface Props {
  prev: string;
  next: string;
  onNavigate: (symbol: string) => void;
}

function PocketNavigation({ prev, next, onNavigate }: Props): JSX.Element {
  const classes = useStyles();

  return (
    <div className={classes.navigation}>
      <NavButton aria-label="Previous currency" onClick={(): void => onNavigate(prev)}>
        <PrevIcon fontSize="large" />
        <span style={{ marginRight: 10 }}>{prev}</span>
      </NavButton>

      <NavButton aria-label="Next currency" onClick={(): void => onNavigate(next)}>
        <span style={{ marginLeft: 10 }}>{next}</span>
        <NextIcon fontSize="large" />
      </NavButton>
    </div>
  );
}

export default PocketNavigation;
