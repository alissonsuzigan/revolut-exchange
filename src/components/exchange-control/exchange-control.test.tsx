import React from 'react';
import { render, RenderResult, fireEvent, act } from '@testing-library/react';
import ExchangeControl from './exchange-control';
import { PocketProvider } from '../../store/pocket';
import { ExchangeProvider } from '../../store/exchange';
import { walletMock, exchangeMock } from '../../utils/mocks';

const gotoRoute = jest.fn();
exchangeMock.source.value = 100;
exchangeMock.target.value = 120;

const renderWithProvider = (): RenderResult => (
  render(
    <PocketProvider data={walletMock}>
      <ExchangeProvider data={exchangeMock}>
        <ExchangeControl gotoRoute={gotoRoute} />
      </ExchangeProvider>
    </PocketProvider>,
  )
);

describe('<ExchangeCurrency />', (): void => {
  jest.useFakeTimers();

  it('renders properly', (): void => {
    const { container } = renderWithProvider();
    const buttons = container.querySelectorAll('button');
    expect(buttons[0].textContent).toBe('Pockets');
    expect(buttons[1].textContent).toBe('Exchange');
  });

  it('dispatches click event', (): void => {
    const { container } = renderWithProvider();
    const buttons = container.querySelectorAll('button');

    fireEvent.click(buttons[0]);
    expect(gotoRoute).toHaveBeenCalledTimes(1);

    fireEvent.click(buttons[1]);
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(gotoRoute).toHaveBeenCalledTimes(2);
  });
});
