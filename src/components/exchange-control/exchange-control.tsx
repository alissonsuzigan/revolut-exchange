import React, { useState, useEffect } from 'react';
import { Fab, makeStyles } from '@material-ui/core';
import { usePocket } from '../../store/pocket';
import { useExchange } from '../../store/exchange';

const useStyles = makeStyles({
  container: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: 80,
  },
  button: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  active: {
    background: '#1460BD',
    paddingLeft: 30,
    paddingRight: 30,
  },
});

function ExchangeControl({ gotoRoute }: {gotoRoute: (path: string) => void}): JSX.Element {
  const classes = useStyles();
  const { setPocketExchange } = usePocket();
  const { exchange, setValues } = useExchange();
  const [disabled, setDisabled] = useState(true);

  const gotoPocketsRoute = (): void => gotoRoute('/pockets');

  const exchangeCurrencies = (): void => {
    const { source, target } = exchange;
    setPocketExchange(source, target);
    setTimeout(() => {
      setValues(0, 0);
      gotoPocketsRoute();
    }, 1000);
  };

  useEffect(() => {
    const { source, target } = exchange;
    if (source.value === 0 || target.value === 0 || source.symbol === target.symbol) setDisabled(true);
    else setDisabled(false);
    // eslint-disable-next-line
  }, [exchange.source.value, exchange.target.value]);

  return (
    <section className={classes.container}>
      <Fab
        variant="extended"
        className={classes.button}
        onClick={gotoPocketsRoute}
      >
        Pockets
      </Fab>
      <Fab
        color="primary"
        variant="extended"
        className={classes.active}
        onClick={exchangeCurrencies}
        disabled={disabled}
      >
        Exchange
      </Fab>
    </section>
  );
}

export default ExchangeControl;
