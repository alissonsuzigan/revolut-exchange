import React, { ReactNode } from 'react';
import { withStyles } from '@material-ui/core';
import CustomButton from './custom-button';

const CustomNav = withStyles({
  root: { width: 110 },
})(CustomButton);

interface Props {
  className?: string;
  ariaLabel?: string;
  onClick?: () => void;
  children?: ReactNode;
}

function NavButton(props: Props): JSX.Element {
  const {
    className, ariaLabel, onClick, children,
  } = props;

  return (
    <CustomNav className={className} variant="extended" aria-label={ariaLabel} onClick={onClick}>
      {children}
    </CustomNav>
  );
}

export default NavButton;
