import React, { ReactNode } from 'react';
import { Fab, withStyles } from '@material-ui/core';

const CustomFab = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    border: '2px solid rgba(255, 255, 255, 0.9)',
    color: 'rgba(255, 255, 255, 0.9)',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      border: '2px solid rgba(255, 255, 255, 0.9)',
      color: 'rgba(255, 255, 255, 0.9)',
    },
  },
})(Fab);

interface Props {
  className?: string;
  variant?: 'round' | 'extended';
  ariaLabel?: string;
  onClick?: () => void;
  children?: ReactNode;
}

function CustomButton(props: Props): JSX.Element {
  const {
    className, variant, ariaLabel, onClick, children,
  } = props;

  return (
    <CustomFab className={className} variant={variant} aria-label={ariaLabel} onClick={onClick}>
      {children}
    </CustomFab>
  );
}

export default CustomButton;
