import React from 'react';
import { Fab, Typography, makeStyles, withStyles } from '@material-ui/core';
import {
  NavigateNext as NextIcon,
  NavigateBefore as PrevIcon,
  Autorenew as ExchangeIcon,
} from '@material-ui/icons';
import { usePocket } from '../../store/pocket';
import { format } from '../../utils/currency';

const useStyles = makeStyles({
  container: {
    color: '#FFF',
    backgroundImage: 'linear-gradient(to bottom left, #33A5FF, #1460BD)',
    fontSize: '1.5rem',
    display: 'flex',
    flexDirection: 'column',
  },
  currency: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    flex: '1',
  },
  control: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: '80px',
  },
  button: {
    width: 110,
  },
});

const CustomFab = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    border: '2px solid rgba(255, 255, 255, 0.9)',
    color: 'rgba(255, 255, 255, 0.9)',
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.1)',
      border: '2px solid rgba(255, 255, 255, 0.9)',
      color: 'rgba(255, 255, 255, 0.9)',
    },
  },
})(Fab);

function PocketDetail({ gotoRoute }: {gotoRoute: (path: string) => void}): JSX.Element {
  const classes = useStyles();
  const { wallet, getActivePocket, setActivePocket, getPrevNextSymbol } = usePocket();
  const pocket = getActivePocket();
  const formattedCurrency = format(pocket.symbol, pocket.value);
  const { prev, next } = getPrevNextSymbol(wallet.activeKey);

  return (
    <section className={classes.container}>
      <div className={classes.currency}>
        <Typography variant="body2" component="p" align="center" gutterBottom>
          You have:
        </Typography>
        <Typography variant="h4" component="p" align="center" gutterBottom>
          {formattedCurrency}
        </Typography>
        <Typography variant="body1" component="p" align="center">
          {pocket.symbol} - {pocket.label}
        </Typography>
      </div>

      <div className={classes.control}>
        <CustomFab
          className={classes.button}
          variant="extended"
          aria-label="Previous currency"
          onClick={(): void => setActivePocket(prev)}
        >
          <PrevIcon fontSize="large" />
          <span style={{ marginRight: 10 }}>{prev}</span>
        </CustomFab>

        <CustomFab aria-label="Exchange currency" onClick={(): void => gotoRoute('/exchanges')}>
          <ExchangeIcon fontSize="large" />
        </CustomFab>

        <CustomFab
          className={classes.button}
          variant="extended"
          aria-label="Next currency"
          onClick={(): void => setActivePocket(next)}
        >
          <span style={{ marginLeft: 10 }}>{next}</span>
          <NextIcon fontSize="large" />
        </CustomFab>
      </div>
    </section>
  );
}

export default PocketDetail;
