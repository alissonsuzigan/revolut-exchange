import React from 'react';
import { render, fireEvent, RenderResult } from '@testing-library/react';
import { PocketProvider } from '../../store/pocket';
import PocketDetail from './pocket-detail';
import { format } from '../../utils/currency';
import { walletMock } from '../../utils/mocks';
import { Wallet } from '../../types';

const gotoRoute = jest.fn();
const renderWithProvider = (): RenderResult => (
  render(<PocketProvider data={walletMock}><PocketDetail gotoRoute={gotoRoute} /></PocketProvider>)
);

describe('<PocketDetail />', (): void => {
  it('renders properly', (): void => {
    const { container, getByText } = renderWithProvider();
    const buttons = container.querySelectorAll('button');
    const { activeKey, pockets } = walletMock as Wallet;

    // Validating texts
    getByText('You have:');
    getByText(new RegExp(pockets[activeKey].value.toString()));
    getByText(`${activeKey} - ${pockets[activeKey].label}`);

    // Validating buttons
    expect(buttons).toHaveLength(3);
    // Previous currency
    expect(buttons[0].innerHTML).toMatch('USD');
    expect(buttons[0].getAttribute('aria-label')).toBe('Previous currency');
    expect(buttons[0].querySelector('svg')).toBeTruthy();
    // Exchange currency
    expect(buttons[1].getAttribute('aria-label')).toBe('Exchange currency');
    expect(buttons[1].querySelector('svg')).toBeTruthy();
    // Next currency
    expect(buttons[2].innerHTML).toMatch('EUR');
    expect(buttons[2].getAttribute('aria-label')).toBe('Next currency');
    expect(buttons[2].querySelector('svg')).toBeTruthy();
  });

  it('changes active currency', () => {
    const { getByText } = renderWithProvider();
    const { pockets } = walletMock as Wallet;
    // Initial values
    getByText(format('GBP', pockets.GBP.value));
    getByText(`GBP - ${pockets.GBP.label}`);
    // Change currency
    fireEvent.click(getByText('EUR'));
    // Changed values
    getByText(format('EUR', pockets.EUR.value));
    getByText(`EUR - ${pockets.EUR.label}`);
    // Change currency
    fireEvent.click(getByText('GBP'));
    // Changed values
    getByText(format('GBP', pockets.GBP.value));
    getByText(`GBP - ${pockets.GBP.label}`);
  });

  it('navigates to exchange route', () => {
    const { container } = renderWithProvider();
    fireEvent.click(container.querySelectorAll('button')[1]);
    expect(gotoRoute).toBeCalledTimes(1);
  });
});
