import React from 'react';
// Material UI
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
// Configs
import Router from '../../router';
import theme from '../../theme';
import { ExchangeProvider } from '../../store/exchange';
import { PocketProvider } from '../../store/pocket';
import { walletMock } from '../../utils/mocks';

const App = (): JSX.Element => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <PocketProvider data={walletMock}>
      <ExchangeProvider>
        <Router />
      </ExchangeProvider>
    </PocketProvider>
  </ThemeProvider>
);

export default App;
