import React from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';
import ExchangeSource from './exchange-source';
import { PocketProvider } from '../../store/pocket';
import { walletMock } from '../../utils/mocks';

const renderWithProvider = (): RenderResult => (
  render(
    <PocketProvider data={walletMock}>
      <ExchangeSource />
    </PocketProvider>,
  )
);

describe('<ExchangeCurrency />', (): void => {
  const { container } = renderWithProvider();
  const labels = container.querySelectorAll('label');
  const buttons = container.querySelectorAll('button');

  it('renders properly and navigates to EUR pocket', (): void => {
    expect(labels[0].textContent).toBe('GBP');
    expect(labels[1].textContent).toBe('You have £800.00');
    expect(buttons[0].textContent).toBe('USD');
    expect(buttons[1].textContent).toBe('EUR');

    fireEvent.click(buttons[1]);
    expect(labels[0].textContent).toBe('EUR');
    expect(labels[1].textContent).toBe('You have €1,000.00');
    expect(buttons[0].textContent).toBe('GBP');
    expect(buttons[1].textContent).toBe('USD');
  });
});
