import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import ExchangePanel from '../exchange-panel';
import PocketNavigation from '../pocket-navigation';
import { usePocket } from '../../store/pocket';
import { Pocket } from '../../types';
import { useExchange } from '../../store/exchange';

const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateRows: '1fr 2fr auto',
    alignItems: 'center',
    backgroundImage: 'linear-gradient(to bottom left, #33A5FF, #1460BD)',
    color: '#FFF',
  },
  title: {
    fontSize: '.9rem',
    fontWeight: 'normal',
    textAlign: 'center',
    margin: '10px 0',
  },
});

function ExchangeSource(): JSX.Element {
  const classes = useStyles();
  const timeToUpdate = 10000;
  const { exchange, setSource, fetchRates } = useExchange();
  const { wallet, getPrevNextSymbol, getPocketBySymbol } = usePocket();
  const activePocket = getPocketBySymbol(exchange.source.symbol);
  const [pocket, setPocket] = useState<Pocket>(activePocket);
  const { prev, next } = getPrevNextSymbol(pocket.symbol);

  const onNavigate = (symbol: string): void => {
    const pkt = getPocketBySymbol(symbol);
    const sourceValue = (exchange.source.value < pkt.value) ? exchange.source.value : pkt.value;
    setPocket(pkt);
    setSource(symbol, sourceValue);
  };

  // Effect for fetching rates
  useEffect(() => {
    const interval = setInterval(() => {
      fetchRates(pocket.symbol, Object.keys(wallet.pockets));
    }, timeToUpdate);
    fetchRates(pocket.symbol, Object.keys(wallet.pockets));

    return (): void => clearInterval(interval);
    // eslint-disable-next-line
  }, [pocket.symbol, wallet.pockets]);

  return (
    <section className={classes.container}>
      <header><h1 className={classes.title}>Exchange from</h1></header>
      <ExchangePanel data={pocket} inputValue={exchange.source.value} updateValue={setSource} />
      <PocketNavigation prev={prev} next={next} onNavigate={onNavigate} />
    </section>
  );
}

export default ExchangeSource;
