import React from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';
import ExchangeTarget from './exchange-target';
import { ExchangeProvider } from '../../store/exchange';
import { PocketProvider } from '../../store/pocket';
import { walletMock, exchangeMock } from '../../utils/mocks';

const renderWithProvider = (): RenderResult => (
  render(
    <PocketProvider data={walletMock}>
      <ExchangeProvider data={exchangeMock}>
        <ExchangeTarget />
      </ExchangeProvider>
    </PocketProvider>,
  )
);

describe('<ExchangeCurrency />', (): void => {
  it('renders properly and navigates to GBP pocket', (): void => {
    const { container } = renderWithProvider();
    const labels = container.querySelectorAll('label');
    const buttons = container.querySelectorAll('button');

    expect(labels[0].textContent).toBe('USD');
    expect(labels[1].textContent).toBe('You have $1,500.00');
    expect(buttons[0].textContent).toBe('EUR');
    expect(buttons[1].textContent).toBe('GBP');
    expect(container.querySelectorAll('span')[0].textContent).toBe('$1.00 = £0.81');

    fireEvent.click(buttons[1]);
    expect(labels[0].textContent).toBe('GBP');
    expect(labels[1].textContent).toBe('You have £800.00');
    expect(buttons[0].textContent).toBe('USD');
    expect(buttons[1].textContent).toBe('EUR');
    expect(container.querySelectorAll('span')[0].textContent).toBe('£1.00 = £1.00');
  });
});
