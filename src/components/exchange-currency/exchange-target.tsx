import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import ExchangePanel from '../exchange-panel';
import PocketNavigation from '../pocket-navigation';
import { usePocket } from '../../store/pocket';
import { useExchange } from '../../store/exchange';
import { format } from '../../utils/currency';
import { Pocket } from '../../types';

const useStyles = makeStyles({
  container: {
    display: 'grid',
    gridTemplateRows: '1fr 2fr auto',
    alignItems: 'center',
    backgroundImage: 'linear-gradient(to bottom left, #33A5FF, #1460BD)',
    color: '#FFF',
  },
  title: {
    fontSize: '.9rem',
    fontWeight: 'normal',
    textAlign: 'center',
    margin: '10px 0',
  },
});

function ExchangeTarget(): JSX.Element {
  const classes = useStyles();
  const { exchange, setTarget, setValues } = useExchange();
  const { getPrevNextSymbol, getPocketBySymbol } = usePocket();
  const targetPocket = getPocketBySymbol(exchange.target.symbol);
  const [pocket, setPocket] = useState<Pocket>(targetPocket);
  const { prev, next } = getPrevNextSymbol(pocket.symbol);

  const onNavigate = (symbol: string): void => {
    setPocket({ ...getPocketBySymbol(symbol) });
    setTarget(symbol, exchange.target.value);
  };

  // Effect exchange values
  const setExchangeValues = (): void => {
    const { value } = exchange.source;
    const rate = exchange.rates[exchange.target.symbol];
    const targetValue = parseFloat((value * rate).toFixed(2));
    setValues(value, targetValue);
  };

  useEffect(() => {
    setExchangeValues();
    // eslint-disable-next-line
  }, [exchange.source.symbol, exchange.source.value, exchange.target.symbol, exchange.rates]);

  const getExchangeRate = (): string | undefined => {
    if (Object.keys(exchange.rates).length === 0) return undefined;

    const rate = exchange.rates[exchange.target.symbol];
    const targetRate = parseFloat((1 / rate).toFixed(2));
    const formatSource = format(exchange.target.symbol, 1);
    const formatTarget = format(exchange.source.symbol, targetRate);
    return `${formatSource} = ${formatTarget}`;
  };

  return (
    <section className={classes.container}>
      <header><h1 className={classes.title}>Exchange to</h1></header>
      <ExchangePanel
        data={pocket}
        exchangeRate={getExchangeRate()}
        inputValue={exchange.target.value}
        updateValue={setTarget}
        disabled
      />
      <PocketNavigation prev={prev} next={next} onNavigate={onNavigate} />
    </section>
  );
}

export default ExchangeTarget;
