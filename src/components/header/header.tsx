import React from 'react';
import { AppBar, Toolbar, makeStyles } from '@material-ui/core';
import logo from './revolut-250-250.png';

const useStyles = makeStyles({
  toolbar: {
    justifyContent: 'center',
  },
  figure: {
    width: 110,
    height: 50,
  },
  logo: {
    position: 'relative',
    top: -30,
    width: '100%',
    height: 'auto',
  },
});

function Header(): JSX.Element {
  const classes = useStyles();

  return (
    <AppBar position="relative" color="default">
      <Toolbar className={classes.toolbar}>
        <div className={classes.figure}>
          <img className={classes.logo} src={logo} alt="Revolut logo" />
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
