import React from 'react';
import { render } from '@testing-library/react';
import Header from './header';

describe('<Header />', (): void => {
  it('renders properly', (): void => {
    const { container } = render(<Header />);
    const image = container.querySelector('img');

    expect(image).toBeTruthy();
    expect(image!.getAttribute('src')).toBe('revolut-250-250.png');
    expect(image!.getAttribute('alt')).toBe('Revolut logo');
  });
});
