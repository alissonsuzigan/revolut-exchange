import React from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';
import { PocketProvider } from '../../store/pocket';
import PocketList from './pocket-list';
import { walletMock } from '../../utils/mocks';
import { Wallet } from '../../types';

const renderWithProvider = (): RenderResult => (
  render(<PocketProvider data={walletMock}><PocketList /></PocketProvider>)
);

describe('<PocketList />', (): void => {
  it('renders properly', (): void => {
    const { container, getByText } = renderWithProvider();
    const { pockets } = walletMock as Wallet;

    // Validating heading
    expect(container.querySelector('h2')).toBeTruthy();
    getByText('Available currencies');

    // Validating currency list
    expect(container.querySelector('nav')).toBeTruthy();
    getByText(`GBP - ${pockets.GBP.label}`);
    getByText(`EUR - ${pockets.EUR.label}`);
    getByText(`USD - ${pockets.USD.label}`);

    fireEvent.click(getByText(`EUR - ${pockets.EUR.label}`));
  });
});
