import React, { Fragment } from 'react';
import {
  Divider, List, ListItem, ListItemText, makeStyles, Typography,
} from '@material-ui/core';
import { usePocket } from '../../store/pocket';
import { Pocket } from '../../types';

const useStyles = makeStyles({
  title: {
    background: '#CCC',
    padding: 10,
  },
});

function PocketList(): JSX.Element {
  const classes = useStyles();
  const { wallet, setActivePocket } = usePocket();

  const renderPocketList = (pocket: Pocket): JSX.Element => {
    const { symbol, label } = pocket;
    return (
      <Fragment key={symbol}>
        <ListItem button onClick={(): void => setActivePocket(symbol)}>
          <ListItemText primary={`${symbol} - ${label}`} />
        </ListItem>
        <Divider />
      </Fragment>
    );
  };

  return (
    <section id="pocket-list">
      <Typography variant="body1" component="h2" align="center" className={classes.title}>
        Available currencies
      </Typography>
      <List component="nav" aria-label="main mailbox folders">
        {Object.values(wallet.pockets).map(renderPocketList)}
      </List>
    </section>
  );
}

export default PocketList;
