import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// Router components
import Pockets from './routes/pockets';
import Exchanges from './routes/exchanges';

const Router = (): JSX.Element => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Pockets} />
      <Route path="/pockets" component={Pockets} />
      <Route path="/exchanges" component={Exchanges} />
      <Route component={Pockets} />
    </Switch>
  </BrowserRouter>
);

export default Router;
